package be.andy.annotationforward;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class StoreData {

    public static void main(String[] args) {
//    	old way an cfg.xml file must be created:
//		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("some.cfg.xml").build();
//		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
//		SessionFactory factory = meta.getSessionFactoryBuilder().build();

//		Using session taken from entity manager
//        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        Session session = entityManager.unwrap(Session.class);
//        Transaction t = session.beginTransaction();
//
//        Employee e1 = new Employee();
//        e1.setFirstName("Gaurav66");
//        e1.setLastName("Chawla");
//
//        int id = (int) session.save(e1);
//        t.commit();
//        System.out.println("id after save " + id);
//        System.out.println("successfully saved");
//        session.close();
//        entityManagerFactory.close();

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            Employee e1 = new Employee();
            e1.setFirstName("Andy");
            e1.setLastName("Dandy");


            entityManager.persist(e1);
            System.out.println(e1.getId());


            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }

    }

}
