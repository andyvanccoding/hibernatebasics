package be.andy;

import be.andy.entity.Address;
import be.andy.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class List {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        Address a1 = new Address();
        a1.setE_pincode(201301);
        a1.setE_city("Noida");
        a1.setE_state("Uttar Pradesh");

        Address a2 = new Address();
        a2.setE_pincode(302001);
        a2.setE_city("Jaipur");
        a2.setE_state("Rajasthan");

        Employee e1 = new Employee();
        e1.setE_name("Vijay");
        e1.getAddress().add(a1);
        e1.getAddress().add(a2);

        Employee e2 = new Employee();
        e2.setE_name("John");
        e2.getAddress().add(a2);

        try {
            transaction.begin();

            entityManager.persist(e1);
            entityManager.persist(e2);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
