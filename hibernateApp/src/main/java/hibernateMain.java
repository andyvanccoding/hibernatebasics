import entity.Employee;

import javax.persistence.*;

public class hibernateMain {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

//            Persisting an object JPA
//            Employee andy = new Employee();
//            andy.setId(5);
//            andy.setFirstName("Andy");
//            andy.setLastName("Van Camp");
//            entityManager.persist(andy);

//            @NamedQuery example
//            TypedQuery<Employee> empByDeptQuery = entityManager.createNamedQuery("Employee.byDept", Employee.class);
//            empByDeptQuery.setParameter(1, "Java Advocacy");
//            for (Employee employee : empByDeptQuery.getResultList()) {
//                System.out.println(employee);
//            }

            Query countEmpByDept = entityManager.createNativeQuery(
                    "select count(*) from employee inner join department d on employee.department_id = d.id where d.name=:deptName"
            );
            countEmpByDept.setParameter("deptName", "Java Advocacy");
            System.out.println("there are " + countEmpByDept.getSingleResult() + " Java Advocates");

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
