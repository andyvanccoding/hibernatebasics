package be.andy.OneToOne.OnForeignKey.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class LibraryOTO {
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long b_id;
    private String b_name;
    private LocalDate createdOn;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId //now id property serves as PK & FK (no more generating value on @id)
    private StudentOTO stud;

    public LibraryOTO(Long b_id, String b_name, StudentOTO stud) {
        super();
        this.b_id = b_id;
        this.b_name = b_name;
        this.stud = stud;
    }

    public LibraryOTO() {
        super();
        this.createdOn = LocalDate.now();
    }

    public Long getB_id() {
        return b_id;
    }

    public void setB_id(Long b_id) {
        this.b_id = b_id;
    }

    public String getB_name() {
        return b_name;
    }

    public void setB_name(String b_name) {
        this.b_name = b_name;
    }

    public StudentOTO getStud() {
        return stud;
    }

    public void setStud(StudentOTO stud) {
        this.stud = stud;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }
}
