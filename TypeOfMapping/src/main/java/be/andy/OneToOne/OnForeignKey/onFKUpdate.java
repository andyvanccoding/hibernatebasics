package be.andy.OneToOne.OnForeignKey;

import be.andy.OneToOne.OnForeignKey.entity.LibraryOTO;
import be.andy.OneToOne.OnForeignKey.entity.StudentOTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class onFKUpdate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            LibraryOTO libraryOTO = entityManager.find(LibraryOTO.class, 1L);
            StudentOTO studentOTO = entityManager.find(StudentOTO.class, 1L);

            entityManager.remove(libraryOTO);

            LibraryOTO libraryOTO1 = new LibraryOTO();
            libraryOTO1.setStud(studentOTO);
            libraryOTO1.setB_name("this is updated via hibernate with change of id");
            entityManager.persist(libraryOTO1);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
