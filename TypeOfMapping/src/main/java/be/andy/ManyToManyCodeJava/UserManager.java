package be.andy.ManyToManyCodeJava;

import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class UserManager {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // obtains the session
        Session session = entityManager.unwrap(Session.class);
        session.beginTransaction();

        Group groupAdmin = new Group("Administrator Group");
        Group groupGuest = new Group("Guest Group");

        User user1 = new User("Tom", "tomcat", "tom@codejava.net");
        User user2 = new User("Mary", "mary", "mary@codejava.net");

        groupAdmin.addUser(user1);
        groupAdmin.addUser(user2);

        groupGuest.addUser(user1);

        user1.addGroup(groupAdmin);
        user2.addGroup(groupAdmin);
        user1.addGroup(groupGuest);

        session.save(groupAdmin);
        session.save(groupGuest);

        session.getTransaction().commit();
        session.close();
    }

}
