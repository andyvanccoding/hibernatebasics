package be.andy.manytomanyToSelf;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Set;

public class UserSearch {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        UserSelf userSelf = entityManager.find(UserSelf.class, 1L);
        UserSelf userOther = entityManager.find(UserSelf.class, 2L);

        //find userself friends:
        Set<UserSelf> user = userSelf.getUserTo();
        //find who userself is friend of:
        Set<UserSelf> userOp = userSelf.getUserFrom();

        //find who is userother friend of:
        Set<UserSelf> userFriend = userOther.getUserFrom();
        //find user other friends:
        Set<UserSelf> userFriendOp = userOther.getUserTo();


        System.out.println("Friend of user return: ");
        user.forEach(System.out::println);
        System.out.println("User is friend of return: ");
        userOp.forEach(System.out::println);

        System.out.println("User friend is friend of user: ");
        userFriend.forEach(System.out::println);
        System.out.println("UserFriend has friends: ");
        userFriendOp.forEach(System.out::println);
    }
}
