package be.andy.ManyToMany.BiDirectional;

import be.andy.ManyToMany.BiDirectional.entity.LibraryMTMBi;
import be.andy.ManyToMany.BiDirectional.entity.StudentMTMBi;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;

public class OnFKCreate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            StudentMTMBi st1 = new StudentMTMBi("Andy", null);
            StudentMTMBi st2 = new StudentMTMBi("Thao Ly", null);

            LibraryMTMBi lib1 = new LibraryMTMBi("Data Structure", null);
            LibraryMTMBi lib2 = new LibraryMTMBi("DBMS", null);

            ArrayList<StudentMTMBi> alStudents = new ArrayList<>();
            ArrayList<LibraryMTMBi> alLibrarys = new ArrayList<>();

            alStudents.add(st1);
            alStudents.add(st2);

            alLibrarys.add(lib1);
            alLibrarys.add(lib2);

            st1.setLibraryMTM(alLibrarys);
            lib1.setStudentMTM(alStudents);
            st2.setLibraryMTM(alLibrarys);
            lib2.setStudentMTM(alStudents);

            entityManager.persist(st1);
            entityManager.persist(st2);

            entityManager.persist(lib1);
            entityManager.persist(lib2);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
