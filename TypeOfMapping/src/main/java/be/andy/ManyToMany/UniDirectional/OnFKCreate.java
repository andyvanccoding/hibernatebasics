package be.andy.ManyToMany.UniDirectional;

import be.andy.ManyToMany.UniDirectional.entity.LibraryMTMUni;
import be.andy.ManyToMany.UniDirectional.entity.StudentMTMUni;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;

public class OnFKCreate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            StudentMTMUni st1 = new StudentMTMUni("Andy", null);
            StudentMTMUni st2 = new StudentMTMUni("Thao Ly", null);

            LibraryMTMUni lib1 = new LibraryMTMUni("Data Structure", null);
            LibraryMTMUni lib2 = new LibraryMTMUni("DBMS", null);

            ArrayList<StudentMTMUni> alStudents = new ArrayList<>();
            ArrayList<LibraryMTMUni> alLibrarys = new ArrayList<>();

            alStudents.add(st1);
            alStudents.add(st2);

            alLibrarys.add(lib1);
            alLibrarys.add(lib2);

            st1.setLibraryMTM(alLibrarys);
            st2.setLibraryMTM(alLibrarys);

            entityManager.persist(st1);
            entityManager.persist(st2);

            entityManager.persist(lib1);
            entityManager.persist(lib2);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
