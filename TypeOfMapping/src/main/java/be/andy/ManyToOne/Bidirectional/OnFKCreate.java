package be.andy.ManyToOne.Bidirectional;

import be.andy.ManyToOne.Bidirectional.entity.LibraryMTOBi;
import be.andy.ManyToOne.Bidirectional.entity.StudentMTOBi;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class OnFKCreate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            LibraryMTOBi lib = new LibraryMTOBi();
            lib.setB_name("Data Structure");

            StudentMTOBi st1 = new StudentMTOBi();
            st1.setS_name("Andy");

            StudentMTOBi st2 = new StudentMTOBi();
            st2.setS_name("Thao Ly");

            lib.addStudent(st1);
            lib.addStudent(st2);

            entityManager.persist(st1);
            entityManager.persist(st2);
            entityManager.persist(lib);


            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }

    }
}
