package be.andy.ManyToOne.Unidirectional;

import be.andy.ManyToOne.Unidirectional.entity.LibraryMTOUni;
import be.andy.ManyToOne.Unidirectional.entity.StudentMTOUni;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class OnFKCreate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            LibraryMTOUni lib = new LibraryMTOUni();
            lib.setB_name("Data Structure");

            entityManager.persist(lib);

            StudentMTOUni st1 = new StudentMTOUni();
            st1.setS_name("Andy");
            st1.setLibraryMTO(lib);


            StudentMTOUni st2 = new StudentMTOUni();
            st2.setS_name("Thao Ly");
            st2.setLibraryMTO(lib);

            entityManager.persist(st1);

            entityManager.persist(st2);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }

    }
}
