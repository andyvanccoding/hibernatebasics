package be.andy.ManyToOne.Unidirectional.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class StudentMTOUni {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int s_id;
    private String s_name;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private LibraryMTOUni libraryMTOUni;

    public LibraryMTOUni getLibraryMTO() {
        return libraryMTOUni;
    }

    public void setLibraryMTO(LibraryMTOUni libraryMTOUni) {
        this.libraryMTOUni = libraryMTOUni;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }


}
