package be.andy.ManyToOne.Unidirectional.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LibraryMTOUni {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int b_id;
    private String b_name;

    public LibraryMTOUni(int b_id, String b_name, StudentMTOUni stud) {
        super();
        this.b_id = b_id;
        this.b_name = b_name;
    }

    public LibraryMTOUni() {
        super();
    }

    public int getB_id() {
        return b_id;
    }

    public void setB_id(int b_id) {
        this.b_id = b_id;
    }

    public String getB_name() {
        return b_name;
    }

    public void setB_name(String b_name) {
        this.b_name = b_name;
    }
}


