package be.andy.OneToMany.biderectional.onFk.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StudentOTMBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long s_id;
    private String s_name;

    @OneToMany(mappedBy = "studentOTMBi", orphanRemoval = true, cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<LibraryOTMBi> books_issued = new ArrayList<>();

    public StudentOTMBi() {
    }

    public Long getS_id() {
        return s_id;
    }

    public void setS_id(Long s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public List<LibraryOTMBi> getBooks_issued() {
        return books_issued;
    }

    public void setBooks_issued(List<LibraryOTMBi> books_issued) {
        this.books_issued = books_issued;
    }

    public void addBook(LibraryOTMBi libraryOTMBi) {
        books_issued.add(libraryOTMBi);
        libraryOTMBi.setStudentOTM(this);
    }

    public void removeBook(LibraryOTMBi libraryOTMBi) {
        books_issued.remove(libraryOTMBi);
        libraryOTMBi.setStudentOTM(null);
    }
}
