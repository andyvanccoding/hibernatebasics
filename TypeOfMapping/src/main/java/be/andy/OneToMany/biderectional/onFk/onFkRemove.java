package be.andy.OneToMany.biderectional.onFk;

import be.andy.OneToMany.biderectional.onFk.entity.LibraryOTMBi;
import be.andy.OneToMany.biderectional.onFk.entity.StudentOTMBi;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class onFkRemove {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            StudentOTMBi studentOTMBi = entityManager.find(StudentOTMBi.class, 1L);
            LibraryOTMBi libraryOTMBi = studentOTMBi.getBooks_issued().get(0);
            studentOTMBi.removeBook(libraryOTMBi);

            transaction.commit();
        } finally {
            if(transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
