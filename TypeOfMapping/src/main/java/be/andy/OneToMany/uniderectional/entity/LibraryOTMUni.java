package be.andy.OneToMany.uniderectional.entity;

import javax.persistence.*;

@Entity
public class LibraryOTMUni {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long b_id;
    private String b_name;

    public LibraryOTMUni() {
        super();
    }

    public Long getB_id() {
        return b_id;
    }

    public void setB_id(Long b_id) {
        this.b_id = b_id;
    }

    public String getB_name() {
        return b_name;
    }

    public void setB_name(String b_name) {
        this.b_name = b_name;
    }

}


