import entity.Beer;
import org.hibernate.CacheMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


public class SessionScroll {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction txn = entityManager.getTransaction();
        ScrollableResults scrollableResults = null;

        try {
            txn.begin();

            int batchSize = 25;

            Session session = entityManager.unwrap(Session.class);

            scrollableResults = session
                    .createQuery("select b from Beer b")
                    .setCacheMode(CacheMode.IGNORE)
                    .scroll(ScrollMode.FORWARD_ONLY);


            int count = 0;
            while (scrollableResults.next()) {
                Beer beer = (Beer) scrollableResults.get(0);
//                System.out.println(beer);
                processBeer(session, beer);
                if (++count % batchSize == 0) {
                    //flush a batch of updates and release memory:
                    entityManager.flush();
                    entityManager.clear();
                }
            }

            txn.commit();
        } catch (RuntimeException e) {
            if (txn != null && txn.isActive()) txn.rollback();
            throw e;
        } finally {
            if (scrollableResults != null) {
                scrollableResults.close();
            }
            if (entityManager != null) {
                entityManager.close();
            }
        }


    }

    private static void processBeer(Session session, Beer beer) {
        beer.setStock(150);
        session.save(beer);
    }


}
