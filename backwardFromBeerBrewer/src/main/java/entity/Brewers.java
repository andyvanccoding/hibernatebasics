package entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Brewers {
    private int id;
    private String name;
    private String address;
    private String zipCode;
    private String city;
    private Integer turnover;
    private Collection<Beers> beersById;

    @Id
    @Column(name = "Id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "ZipCode")
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Basic
    @Column(name = "City")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "Turnover")
    public Integer getTurnover() {
        return turnover;
    }

    public void setTurnover(Integer turnover) {
        this.turnover = turnover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Brewers brewers = (Brewers) o;

        if (id != brewers.id) return false;
        if (name != null ? !name.equals(brewers.name) : brewers.name != null) return false;
        if (address != null ? !address.equals(brewers.address) : brewers.address != null) return false;
        if (zipCode != null ? !zipCode.equals(brewers.zipCode) : brewers.zipCode != null) return false;
        if (city != null ? !city.equals(brewers.city) : brewers.city != null) return false;
        if (turnover != null ? !turnover.equals(brewers.turnover) : brewers.turnover != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (turnover != null ? turnover.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "brewersByBrewerId")
    public Collection<Beers> getBeersById() {
        return beersById;
    }

    public void setBeersById(Collection<Beers> beersById) {
        this.beersById = beersById;
    }
}
