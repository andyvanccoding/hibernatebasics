package entity;

import javax.persistence.*;
import java.util.Arrays;

@Entity
public class Beers {
    private int id;
    private String name;
    private Double price;
    private Integer stock;
    private Double alcohol;
    private Integer version;
    private byte[] image;
    private Brewers brewersByBrewerId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Price")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Basic
    @Column(name = "Stock")
    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Basic
    @Column(name = "Alcohol")
    public Double getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Double alcohol) {
        this.alcohol = alcohol;
    }

    @Basic
    @Column(name = "Version")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Basic
    @Column(name = "Image")
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Beers beers = (Beers) o;

        if (id != beers.id) return false;
        if (name != null ? !name.equals(beers.name) : beers.name != null) return false;
        if (price != null ? !price.equals(beers.price) : beers.price != null) return false;
        if (stock != null ? !stock.equals(beers.stock) : beers.stock != null) return false;
        if (alcohol != null ? !alcohol.equals(beers.alcohol) : beers.alcohol != null) return false;
        if (version != null ? !version.equals(beers.version) : beers.version != null) return false;
        if (!Arrays.equals(image, beers.image)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (stock != null ? stock.hashCode() : 0);
        result = 31 * result + (alcohol != null ? alcohol.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }

    @Override
    public String toString() {
        return "Beers{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", alcohol=" + alcohol +
                '}';
    }

    @ManyToOne
    @JoinColumn(name = "BrewerId", referencedColumnName = "Id")
    public Brewers getBrewersByBrewerId() {
        return brewersByBrewerId;
    }

    public void setBrewersByBrewerId(Brewers brewersByBrewerId) {
        this.brewersByBrewerId = brewersByBrewerId;
    }
}
